/* ****************************************************************************************
UNIX Shell with History Feature. (History features.)
Project 1 from Ch. 3 of the Dinosaur Book
Built by: 
Jon Patton, Katherine Thamarus, and Chi Duong

For:
TU COSC 439, Sept-Dec 2018
Dr. Tang

References.
Converting a string to an integer safely: 
https://en.cppreference.com/w/c/string/byte/strtoimax
**************************************************************************************** */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <string.h>
#include <inttypes.h>
#include "osh.h"
        
//For managing the command history array.
int historyIndex;   //Current top index
int historyMax;     //Highest possible index (size of the array)

//Initialize the parts of the history and dynamically allocate the array to hold commands.
void initializeHistory()
{
    commandHistory = (char **) calloc(INITIAL_HIST_SIZE, sizeof(char*));
    historyIndex = -1;
    historyMax = INITIAL_HIST_SIZE;
}

//Bookkeeping for the command history array.
void destroyHistory()
{
    destroyStringArray(commandHistory, historyIndex);
}

//Called by invoker when the first character of the string is an exclamation point.
int historyParser(char *historyItem)
{
    int n;          //For the string-to-number conversion
    char *endptr;   //End pointer for the string-to-number conversion

    //Strip the exclamation point from the string.
    char *h = (char *) calloc(MAX_LINE, sizeof(char));
    for (int i = 1; i < MAX_LINE; i++)
    {
        h[i-1] = historyItem[i];
    }

    //If there's nothing left, get outta here.
    if (h[0] == '\0')
    {
        return -1;
    }
    //Got here from !!
    else if (strncasecmp(h, "!", MAX_LINE) == 0)
    {
        if (args != NULL)
        {
            destroyStringArray(args, sizeof(args));
            args = NULL;
        }
        initializeArgs();
        processTokens(getCommandFromHistory(0), args);
    }
    //User will enter a number that's one too big.
    else
    {
        n = strtoimax(h, &endptr, 10) - 1;
        if (endptr != NULL && (n >= 0 && n <= historyIndex))
        {
            if (args != NULL)
            {
                destroyStringArray(args, sizeof(args));
                args = NULL;
            }
            initializeArgs();
            processTokens(getSpecificCommandByIndex(n), args);
        }
        else
        {
            printf("Error: %i is not a valid history item.\n", n + 1);
            return -1;
        }
    }
    //Check if the first character is an exclamation point. If it
    //is, then execute the first command in the stack.

    free(h);
    return invoker(args);
}

//Adds a string to the command history. (Unparsed user input.)
int addToCommandHistory(char *s)
{
    //Don't add the command if it's a request for history or a blank.
    if (s[0] == '!' || s[0] == '\n')
    {
        return 0;
    }
    if (historyIndex == historyMax - 1)
    {
        commandHistory = doubleCapacityOfStringArray(commandHistory, historyMax);
        historyMax = historyMax * 2;
    }

    historyIndex++;
    commandHistory[historyIndex] = strncpy((char *) calloc(MAX_LINE, sizeof(char)), s, MAX_LINE);
    if (commandHistory[historyIndex] != NULL)
    {
        return 1;
    }
    else
    {
        printf("Something's wrong");
        return -1;
    }
}

//Returns the command n steps down from the most recent.
char* getCommandFromHistory(int n)
{
    if (historyIndex - n < 0 || n > historyMax || historyIndex == -1)
    {
        return NULL; // Asked for a value out of bounds of the array, or it's empty.
    }
	return getSpecificCommandByIndex(historyIndex - n);
}

//Returns the command at absolute index n.
char* getSpecificCommandByIndex(int n)
{
	if (n < 0 || n > historyMax || historyIndex == -1)
	{
		return NULL; // Asked for a value out of bounds of the array, or it's empty.
	}
	return commandHistory[n];
}

//Prints the contents of the command history to the console.
void printCommandHistory()
{
    if (commandHistory[0] == NULL)
    {
        printf("No commands in history.\n");
    }
    for (int i = 0; i < 10; i++)
    {
        char* s = getCommandFromHistory(i);
        if (s != NULL)
        {
            printf("%i  %s", historyIndex - i + 1, s);
        }
    }
}
