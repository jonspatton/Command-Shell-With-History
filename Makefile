CC=gcc
CFLAGS=-I.
DEPS = osh.h
OBJ = osh.o oshTools.o invoker.o history.o getch.o arrayTools.o

%.o: %.c $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS)

osh: $(OBJ)
	$(CC) -o $@ $^ $(CFLAGS)
	make clearobjfiles

clearobjfiles:
	rm -f *.o

clean:
	rm osh
	rm -f $(ODIR)*.o *~ core $(INCDIR)/*~ 