/* ****************************************************************************************
UNIX Shell with History Feature. (Execution function.)
Project 1 from Ch. 3 of the Dinosaur Book
Built by: 
Jon Patton, Katherine Thamarus, and Chi Duong

For:
TU COSC 439, Sept-Dec 2018
Dr. Tang
**************************************************************************************** */

#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "osh.h"

//Takes a set of arguments and either performs a custom command execution or
//passes the arguments to execvp if it's an executable system command.
int invoker(char **args)
{
    int e = 0;
    /* *******************************************************************
    Custom commands.
    ******************************************************************* */
   if (strncasecmp(args[0], EXITCMD, MAX_LINE) == 0)
   {
       return 0;
   }
    //history
    else if (strncasecmp(args[0], HISTORYCMD, MAX_LINE) == 0)
    {
        printCommandHistory();
        
        return 0;
    }
    else if (args[0][0] == '!')
    {
        //execute something from history.
        return historyParser(args[0]);
    }

    //Other commands that cannot be executed by execvp go here.
    else if (strncasecmp(args[0], TIMECMD, MAX_LINE) == 0)
    {
        system("./custom/time");

        //OR Make time.c's time function part of this program and don't call system.
        
        return 0;
    }
    else if (strncasecmp(args[0], CHDIRCMD, MAX_LINE) == 0)
    {
        if (pid == 0)
        {
            return 0;
        }
        char *cmd = concat(args[0], " ", getLengthOfString(args[0]), 1);
        cmd = concat(cmd, args[1], getLengthOfString(cmd), getLengthOfString(args[1]));
        return chdir(args[1]);
    }
    /* *******************************************************************
    Built-in system commands are executed by execvp.
    ******************************************************************* */
    else
    {
        return execvp(args[0], args);
    }
}