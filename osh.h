/* ****************************************************************************************
UNIX Shell with History Feature. (Header file)
Project 1 from Ch. 3 of the Dinosaur Book
Built by: 
Jon Patton, Katherine Thamarus, and Chi Duong

For:
TU COSC 439, Sept-Dec 2018
Dr. Tang
**************************************************************************************** */

#include <stdbool.h>
#include <limits.h>

//Maximum line length for the shell.
#define MAX_LINE 80
#define ARGSLEN (MAX_LINE/2 + 1)
#define INITIAL_HIST_SIZE 10

//Custom commands go here. The args get compared to them and
//the function won't use execvp to run them.
#define HISTORYCMD "history"
#define EXITCMD "exit"
#define TIMECMD "time"
#define CHDIRCMD "cd"

#define CUSTOMCMDFOLDER "custom/"
#define SYSTEMCMDFOLDER "/user/bin"

//globally accessible variables.
bool waitFlag;
char **args;
char** commandHistory;
int pid;

//Prototypes

//Osh Tools
void initializeArgs();
void echoTokens(char **args);
char* prompt(char *input);
void processTokens(char *input, char **args);

//Invoker stuff.
int invoker(char **args);

//History stuff
void initializeHistory();
int historyParser(char *historyItem);
char* getSpecificCommandByIndex(int n);
char* getCommandFromHistory(int n);
void printCommandHistory();
int addToCommandHistory(char *s);
void destroyHistory();
int getch(void);

//arrayTools
char** doubleCapacityOfStringArray(char **a, unsigned long len);
void cleanCharArray(char *c, unsigned long len);
void cleanCharStarArray(char **c, unsigned long len);
void destroyStringArray(char ** a, unsigned long len);
int getLengthOfString(char* a);
char* trimArrayFromChar(char *a, int len, char c, int instances);
char* concat(char *a, char *b, int lena, int lenb);