/* ****************************************************************************************
UNIX Shell with History Feature. (Tools used by the main program for parsing.)
Project 1 from Ch. 3 of the Dinosaur Book
Built by: 
Jon Patton, Katherine Thamarus, and Chi Duong

For:
TU COSC 439, Sept-Dec 2018
Dr. Tang
**************************************************************************************** */

#include <stdio.h>
#include <unistd.h>
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>
#include "osh.h"

void initializeArgs()
{
    if (args != NULL)
    {
        destroyStringArray(args, sizeof(args));
        args = NULL;
    }
    args = (char **) calloc(ARGSLEN, sizeof(char*));
}

void processTokens(char *input, char **args)
{
    //Allocate some memory for potential tokens.
    char *token = (char *) malloc(MAX_LINE);

    //parse into tokens
    //i: iterator for traversing the input string
    //j: iterator for the token char array
    //k: count for the array of tokens (args)
    for (int i = 0, j = 0, k = 0; i < MAX_LINE; i++)
    {
        //Add characters to the token while we have regular text
                
        //Suggested expansion: enforce only certain characters.
        //We need regular letters and numbers, ' ', '\n', &, !, -,
        //quotes, maybe some more.
        if (input[i] != ' ' && input[i] != '\n')
        {
            token[j] = input[i];
            j++;
        }
        //A space indicates the end of a token.
        //Add a copy of the token to the arguments list,
        //then advance the token counter and clean the
        //token out.
        else if (input[i] == ' ' || input[i] == '\n')
        {
            if (strncmp(token, "&", MAX_LINE) == 0)
            {
                waitFlag = true;
            }
            else
            {
                args[k] = strncpy((char *) calloc(MAX_LINE, sizeof(char)), token, MAX_LINE);
                k = k + 1;
            }
            j = 0;

            //Free the memory assigned to the token and then give the token some memory for the next round.
            free(token);
            token = (char *) calloc(MAX_LINE, sizeof(char));
        }
    }
    free(token);
}

//For testing.
void echoTokens(char **args)
{
    //Testing code: echoes back the contents of the arg array.
    for(int i = 0; i < MAX_LINE; i++)
    {
        if (args[i] == NULL || args[i] == '\0')
        {
            break;
        }
        printf("%s", args[i]);
    }
    printf("\n");
}
