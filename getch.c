/* ****************************************************************************************
From https://ubuntuforums.org/showthread.php?t=554845
and/or https://stackoverflow.com/questions/2984307/how-to-handle-key-pressed-in-a-linux-console-in-c

It appears in the book "C Clearly - Programming With C In Linux and On Raspberry Pi"

The code shows up in multiple places almost identical, and is often cited as "standard"
so I suspect it comes from a book with more provenance than that, but it looks more "bash-y"
than "C-y", and it's not in e.g. K&R or anything like that that I have lying around.

Incredibly convoluted for such a simple little thing!

Used for:
UNIX Shell with History Feature. (Main file.)
Project 1 from Ch. 3 of the Dinosaur Book
TU COSC 439, Sept-Dec 2018
Dr. Tang
**************************************************************************************** */

#include <termios.h>
#include <unistd.h>
#include <assert.h>
#include <string.h>
#include <stdio.h>


int getch(void) 
{
      int c = 0;

      struct termios org_opts, new_opts;
      int res = 0;
          //-----  store old settings -----------
      res = tcgetattr(STDIN_FILENO, &org_opts);
      assert(res == 0);
          //---- set new terminal parms --------
      memcpy(&new_opts, &org_opts, sizeof(new_opts));
      new_opts.c_lflag &= ~(ICANON | ECHO | ECHOE | ECHOK | ECHONL | ECHOPRT | ECHOKE | ICRNL);
      tcsetattr(STDIN_FILENO, TCSANOW, &new_opts);
      c = getchar();
          //------  restore old settings ---------
      res = tcsetattr(STDIN_FILENO, TCSANOW, &org_opts);
      assert(res == 0);
      return c;
}