/* ****************************************************************************************
Built by: 
Jon Patton

Adapted For:
UNIX Shell with History Feature.
Project 1 from Ch. 3 of the Dinosaur Book
TU COSC 439, Sept-Dec 2018
Dr. Tang
**************************************************************************************** */

#include <string.h> 
#include <stdlib.h>

//Sets all entries of an array to \0.
void cleanCharArray(char *c, unsigned long len)
{
    memset(c, 0, len);
}

//Cleans out an array but doesn't destroy it.
void cleanCharStarArray(char **c, unsigned long len)
{
    for (int i = 0; i < len; i++)
    {
        if (c[i])
        {
            free(c[i]);
            c[i] = NULL;
        }
    }
}

//Frees all entries of a string array and then frees the array itself.
void destroyStringArray(char ** a, unsigned long len)
{
    if (a == NULL)
    {
        free(a);
        return;
    }
    for (int i = 0; i < len; i++)
    {
        if (a[i] != NULL)
        {
            free(a[i]);
            a[i] = NULL;
        }
    }
    free(a);
}


//Used to dynamically resize an array of char**s.
char** doubleCapacityOfStringArray(char **a, unsigned long len)
{
	//Allocate space for a new array with double the capacity of the original.
	int newSize = len * 2;
	char **tmp = (char **) calloc(newSize, sizeof(char*));

     // Copy the pointers to the new array.
    for (int i = 0; i < len; i++)
    {
        tmp[i] = a[i];
    }

    //Free the old array's memory
    free(a);
    //Allocate a replacement array for the history.
    a  = (char **) calloc(newSize, sizeof(char*));
    
    //Copy the contents back into it.
    for (int i = 0; i < len; i++)
    {
        a[i] = tmp[i];
    }
    //Free the memory allocated for the temporary array.
    free(tmp);

    return a;
}

int getLengthOfString(char* a)
{
    int size = 0;
    char c = a[0];
    while (c != 0)
    {
        size++;
        c = a[size];
    }
    return size;
}

//Takes a character array, the length of that array, and a
//character to look for the last instance of.
//Useful for trimming the current working directory
//to the last slash
char* trimArrayFromChar(char *a, int len, char c, int instances)
{
    int index;
    int newLen;
    //Find the last index of the character
    for (int i = len; i > 0; i--)
    {
        if (a[i] == c)
        {
            instances--;
            if (instances <= 0)
            {
                index = i + 1;
                newLen = len - i - 1;
                break;
            }
        }
    }
    char* tmp = (char*) calloc(newLen, sizeof(char));

    //copy a into tmp.
    for(int i = 0; i < len - index; i++)
    {
        tmp[i] = a[index + i];
    }
    free(a);
    a = (char*) calloc(newLen, sizeof(char));
    //copy tmp back into a.
    for(int i = 0; i < newLen; i++)
    {
        a[i] = tmp[i];
    }
    free(tmp);
    return a;
}

//concats a to b
char* concat(char *a, char *b, int lena, int lenb)
{
    int catlen = lena + lenb;
    char *newString = (char*) calloc(catlen, sizeof(char));
    for (int i = 0; i < catlen - lenb; i++)
    {
        newString[i] = a[i];
    }
    for(int i = 0; i < lenb; i++)
    {
        newString[i + lena] = b[i];
    }
    return newString;
}