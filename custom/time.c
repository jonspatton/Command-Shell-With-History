// source code https://www.codingunit.com/c-tutorial-how-to-use-time-and-date-in-c

#include <time.h>
#include <stdio.h>

int main(void)
{
    time_t mytime;
    mytime = time(NULL);
    printf("%s",ctime(&mytime)); // human-readable ctime function converts time_t object pointed by a timer
    
    return 0;
    }
