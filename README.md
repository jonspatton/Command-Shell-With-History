# Command Shell With History

See individual files for function descriptions.

The program is used as if it were the BASH shell Terminal: The user types commands and presses enter to attempt to execute them.

History is printed with the command "history". "!!" executes the most recent command, and !n executes command n, if possible.