/* ****************************************************************************************
UNIX Shell with History Feature. (Main file.)
Project 1 from Ch. 3 of the Dinosaur Book
Built by: 
Jon Patton, Katherine Thamarus, and Chi Duong

For:
TU COSC 439, Sept-Dec 2018
Dr. Tang
**************************************************************************************** */

#include <stdio.h>
#include <unistd.h>
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>
#include "osh.h"

bool waitFlag = false;

int main (void)
{
    if (commandHistory == NULL)
    {
        initializeHistory();
    }

    char *input = (char *) calloc(MAX_LINE, sizeof(char));
    bool run = true;

    while(run)
    {

        //Prompt user for input.
        input = prompt(input);
        run = (strncasecmp(input, "exit\n", MAX_LINE) != 0);
        pid = fork();

        //Error in fork.
        if (pid == -1)
        {
            printf("Error generating new process.\n");
        }

        //Child process
        if (pid == 0)
        {
            if (strncasecmp(input, CHDIRCMD, getLengthOfString(CHDIRCMD)) == 0)
            {
                exit(0);
            }

            initializeArgs();

            processTokens(input, args);

            int e = invoker(args);
            
            if (e == -1)
            {
            	printf("%s is an invalid command.\n", args[0]);
                perror(args[0]);
            }
        	exit(e);
        }
        //Parent process
        else
        {
            addToCommandHistory(input);

            //Wait here, to prevent the prompt from appearing before the command output.
            wait(NULL);

            if (strncasecmp(input, CHDIRCMD, getLengthOfString(CHDIRCMD)) == 0 &&
                input[2] == ' ')
            {
                initializeArgs();
                processTokens(input, args);
                invoker(args);
            }

            if (waitFlag)
            {
                //Key listener
                printf("waiting (press any key to continue) ...\n");
                getch();
                waitFlag = false;
            }
            
        }
        //Prepare for the next loop.
        cleanCharArray(input, MAX_LINE);
        printf("\n");
    }

    //Clean up dynamically allocated memory.
    free(input);
    destroyHistory();
    destroyStringArray(args, sizeof(args));
}


//Prompts the user for input.
char* prompt(char *input)
{
    char *cwd = getcwd(NULL, 0);
    printf("osh [%s]-> ", trimArrayFromChar(cwd, getLengthOfString(cwd), '/', 1));

    //Clear the output buffer
    fflush(stdout);

    //User input.
    fgets(input, MAX_LINE, stdin);

    return input;
}