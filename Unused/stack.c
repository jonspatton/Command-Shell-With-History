#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <limits.h> 
//#include <stdbool.h>
#include "osh.h"

// C program for array implementation of stack 
// A structure to represent a stack 
// Basic code from https://www.geeksforgeeks.org/stack-data-structure-introduction-program/

// Adapted by us to dynamically grow the stack when it's full, add peek() function, and to hold and
//manipulate strings instead of ints.

/* ***************************************************************************************************
Boilerplate code
*************************************************************************************************** */
struct Stack
{
	int top;
	unsigned capacity;
	char **array; // array pointer 
};

// function to create a stack of given capacity. It initializes size of stack as 0 
struct Stack* createStack(unsigned capacity)
{
	struct Stack* stack = (struct Stack*) malloc(sizeof(struct Stack));
	stack->capacity = capacity;
	stack->top = -1;
	stack->array = (char**)malloc(stack->capacity * sizeof(char*)); // value returned by malloc treated as a pointer to an integer 
	return stack;
}

void destroyStack(struct Stack* stack)
{
	free(stack->array);
}

// Stack is full when top is equal to the last index 
bool isFull(struct Stack* stack)
{
	return stack->top == stack->capacity - 1; // return last element(top) in a stack
}

// Stack is empty when top is equal to -1 
bool isEmpty(struct Stack* stack)
{
	return stack->top == -1; // return -1 if last element(top) is -1
}

// Function to add an item to stack.  It increases top by 1 
void push(struct Stack* stack, char *item)
{
	if (isFull(stack)) // if stack is full, return last element(top) in a stack, then return
	{
		doubleCapacity(stack);
	}
	//printf("Current addition to history: %s\n", item);

	(stack->top)++;
	stack->array[stack->top] = item; // increases top by 1 first, then assign an item to top
	//printf("%s saved to history\n", peek(stack)); // confirm item assigned
}

// Function to remove an item from stack.  It decreases top by 1 
char* pop(struct Stack* stack)
{
	if (isEmpty(stack))
	{
		return NULL; // if stack empty return null
	}
	return stack->array[stack->top--]; // else return top first, then descreses top by 1
}

/* ***************************************************************************************************
New functions
*************************************************************************************************** */

char* peek(struct Stack* stack)
{
	if (isEmpty(stack))
	{
		return NULL; // if stack empty return null
	}
	return stack->array[stack->top]; // else return top first, and DON'T decrease
}

//Gets a specific entry, if we know its index.
char* getSpecificEntryByIndex(struct Stack* stack, int n)
{
	if (isEmpty(stack))
	{
		return NULL; // if stack empty return snull
	}
	return stack->array[n];
}

//Gets a specific entry, if we know how far down the list it is
//This is useful for actually iterating over the most recent entries,
//which is exactly what we'll need to do.
char* getEntryByDistanceFromTop(struct Stack* stack, int n)
{
	printf("Currently getting array entry %i: %s\n", n, stack->array[n]);
	if (isEmpty(stack))
	{
		return NULL; // if stack empty return smallest integer
	}
	return getSpecificEntryByIndex(stack, (stack -> top) - n);
}

//We need this when we run out of space in the stack. Growth time and space requirements are O(logn).
void doubleCapacity(struct Stack* stack)
{
	//Allocate space for a new array with double the capacity of the original.
	int newSize = (stack -> capacity) * 2;
	//char* arrayNew = (char*)malloc(newSize * sizeof(char));
	char* arrayNew[newSize];
	stack -> array = memcpy(arrayNew, stack -> array, newSize * sizeof(arrayNew));
}

int getTopValue(struct Stack* stack)
{
	printf("top value: %i \n", stack->top);
	return stack->top;
}